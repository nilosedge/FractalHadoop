package net.nilosplace;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import javax.imageio.ImageIO;

import org.jboss.netty.util.internal.ConcurrentHashMap;

public class MovieExtractorProcessor extends Thread {

	private LinkedBlockingQueue<MovieExtractorQueueObject> queuein;
	private ConcurrentHashMap<Long, MovieExtractorQueueObject> queueout;

	public MovieExtractorProcessor(LinkedBlockingQueue<MovieExtractorQueueObject> queuein, ConcurrentHashMap<Long, MovieExtractorQueueObject> queueout) {
		this.queuein = queuein;
		this.queueout = queueout;
	}

	public void run() {
		while(true) {
			try {
				if(queuein.isEmpty() || queueout.size() >= 30) {
					sleep(1000);
				} else {
					MovieExtractorQueueObject o = queuein.remove();
					BufferedImage bi = ImageIO.read(new ByteArrayInputStream(o.getArrayData()));
					o.setImage(bi);
					queueout.put(o.getKey(), o);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}
}
