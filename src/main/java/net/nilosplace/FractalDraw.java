package net.nilosplace;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferStrategy;
import java.util.stream.IntStream;

import javax.swing.JFrame;

import net.nilosplace.Fractal.color.Colorator;
import net.nilosplace.Fractal.function.FractalFunction;
import net.nilosplace.Fractal.function.FractalFunctionParameters;
import net.nilosplace.Fractal.function.FractalWindow;
import net.nilosplace.Fractal.functions.Function2;
import net.nilosplace.Fractal.functions.Function679;
import net.nilosplace.Fractal.functions.Julia;
import net.nilosplace.Fractal.functions.Mandelbrot;

public class FractalDraw extends JFrame implements MouseListener, MouseMotionListener {

	private int w = 1280;
	private int h = 800;

	//private FractalWindow window = new FractalWindow(w, h, 559912.9782739371, 734885.7839845422, 783500, 13700, 1.2);
	private FractalWindow window = new FractalWindow(w, h, 0, 0, 1.2); // Julia Set
	//private FractalWindow window = new FractalWindow(w, h, 0.0016437219687502394, -0.8224676332993273, 1.2);
	
	private FractalFunctionParameters params = new FractalFunctionParameters();
	private FractalFunction f = null;
	private Colorator co = null;
	private BufferStrategy bf = null;
	
	public static void main(String args[]) {
		new FractalDraw();
	}

	public FractalDraw() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(Color.BLACK);
		//setUndecorated(true);
		setSize(w,h);
		addMouseListener(this);
		addMouseMotionListener(this);
		setVisible(true);
		createBufferStrategy(2);
		bf = getBufferStrategy();
		
		params.maxIter = 200;
		params.p1 = -0.7; // Julia Set
		params.p2 = 0.27015; // Julia Set
		//f = new Mandelbrot(params);
		f = new Julia(params);
		co = new Colorator(Colorator.Palette.RAINBOW);
		
		drawStuff();
	}

	private void drawStuff() {
		Graphics g = bf.getDrawGraphics();

		g.setColor(Color.BLACK);
		g.fillRect(0, 0, w, h);

		//params.maxIter = (int)(Math.sqrt(Math.sqrt(window.getScale().y)) + 200);
		//System.out.println(params.maxIter);
		//IntStream.range(0, w).parallel().forEach(i -> {
		for(int i = 0; i < w; i++) {
			//IntStream.range(0, h).parallel().forEach(j -> {
			for(int j = 0; j < h; j++) {
				Point3D pr = f.computePoint(window.getWindowPoint(i, j));
				g.setColor(co.getMUColor(pr, params.maxIter));
				g.fillRect(i, j, 1, 1);
			}
		}

		g.dispose();

		bf.show();

		Toolkit.getDefaultToolkit().sync();
	}

	public void mouseClicked(MouseEvent e) {
		if(e.getButton() == 1) {
			window.zoomOut();
		}
		
		if(e.getButton() == 3) {
			window.zoomIn();
		}
		//System.out.println(window);
		drawStuff();
	}
	public void mousePressed(MouseEvent e) {
		window.pressed(e.getX(), e.getY());
	}

	public void mouseDragged(MouseEvent e) {
		window.dragged(e.getX(), e.getY());
		//System.out.println(window);
		drawStuff();
	}

	public void mouseReleased(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}

}
