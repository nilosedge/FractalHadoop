package net.nilosplace;

import java.util.HashMap;

import nl.basjes.parse.core.Field;

public class ApacheLogRecord {

	private final HashMap<String, String> results = new HashMap<String, String>();

	public void clear() { 
		results.clear(); 
	}

	public HashMap<String, String> getResults() {
		return results;
	}

	@Field({
//		"TIME.STAMP:request.receive.time",
//		"TIME.DAY:request.receive.time.day",
//		"TIME.MONTHNAME:request.receive.time.monthname",
//		"TIME.MONTH:request.receive.time.month",
//		"TIME.WEEK:request.receive.time.weekofweekyear",
//		"TIME.YEAR:request.receive.time.weekyear",
//		"TIME.YEAR:request.receive.time.year",
//		"TIME.HOUR:request.receive.time.hour",
//		"TIME.MINUTE:request.receive.time.minute",
//		"TIME.SECOND:request.receive.time.second",
//		"TIME.MILLISECOND:request.receive.time.millisecond",
//		"TIME.ZONE:request.receive.time.timezone",
//		"TIME.EPOCH:request.receive.time.epoch",
//		"TIME.DAY:request.receive.time.day_utc",
//		"TIME.MONTHNAME:request.receive.time.monthname_utc",
//		"TIME.MONTH:request.receive.time.month_utc",
//		"TIME.WEEK:request.receive.time.weekofweekyear_utc",
//		"TIME.YEAR:request.receive.time.weekyear_utc",
//		"TIME.YEAR:request.receive.time.year_utc",
//		"TIME.HOUR:request.receive.time.hour_utc",
//		"TIME.MINUTE:request.receive.time.minute_utc",
//		"TIME.SECOND:request.receive.time.second_utc",
//		"TIME.MILLISECOND:request.receive.time.millisecond_utc",
		"IP:connection.client.host",
		//"HTTP.USERAGENT:request.user-agent",
		//"NUMBER:connection.client.logname",
		//"HTTP.FIRSTLINE:request.firstline",
		//"HTTP.METHOD:request.firstline.method",
		//"HTTP.URI:request.firstline.uri",
		//"HTTP.PROTOCOL:request.firstline.uri.protocol",
		//"HTTP.USERINFO:request.firstline.uri.userinfo",
		"HTTP.HOST:request.firstline.uri.host",
		//"HTTP.PORT:request.firstline.uri.port",
		//"HTTP.PATH:request.firstline.uri.path",
		//"HTTP.QUERYSTRING:request.firstline.uri.query",
		//"STRING:request.firstline.uri.query.*",
		//"HTTP.REF:request.firstline.uri.ref",
		//"HTTP.PROTOCOL:request.firstline.protocol",
		//"HTTP.PROTOCOL.VERSION:request.firstline.protocol.version",
		//"STRING:connection.client.user",
		//"HTTP.URI:request.referer",
		//"HTTP.PROTOCOL:request.referer.protocol",
		//"HTTP.USERINFO:request.referer.userinfo",
		//"HTTP.HOST:request.referer.host",
		//"HTTP.PORT:request.referer.port",
		//"HTTP.PATH:request.referer.path",
		//"HTTP.QUERYSTRING:request.referer.query",
		//"STRING:request.referer.query.*",
		//"HTTP.REF:request.referer.ref",
		//"STRING:request.status.last",
		//"BYTES:response.body.bytesclf"
		
	})
	public void setValue(final String name, final String value) {
		//System.out.println("SETTER CALLED FOR \"" + name + "\" = \"" + value + "\"");
		results.put(name, value);
	}
	
	


	public String toString() { 
		StringBuilder sb = new StringBuilder(); 
		sb.append(" ----------- BEGIN ----------\n"); 
		for (String key : results.keySet()) { 
			sb.append(key);
			sb.append(" -> ");
			sb.append(results.get(key)).append('\n'); 
		} 
		sb.append(" ------------ END -----------\n"); 
		sb.append("\n"); 
		return sb.toString(); 
	} 
	
	
}
