package net.nilosplace;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;

import javax.imageio.ImageIO;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.util.ReflectionUtils;


public class FractalReader {

	public static void main(String[] args) throws Exception {
		
		Configuration conf = new Configuration();

		Path path = new Path("hdfs://localhost:9000/user/oblod/file_out/part-m-00000");
		SequenceFile.Reader reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(path));
		LongWritable key = (LongWritable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
		BytesWritable value = (BytesWritable) ReflectionUtils.newInstance(reader.getValueClass(), conf);
		
		while (reader.next(key, value)) {
			System.out.println("key : " + key + " - value : " + value.getBytes().length);
			BufferedImage bi = ImageIO.read(new ByteArrayInputStream(value.getBytes()));
			ImageIO.write(bi, "png", new File("/Users/oblod/Desktop/Fractals/" + key + ".png"));
		}
		IOUtils.closeStream(reader);

	}

}
