package net.nilosplace.Fractal.function;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class FractalWindow {

	private double zf = 1.1;

	// Cool Image on desktop background
	private Point2D.Double size = new Double(2560, 1600);
	private Point2D.Double half_size = new Double(0, 0);
	private Point2D.Double scale = new Double(559912.9782739371, 734885.7839845422);
	private Point2D.Double transform = new Double(783500, 13700);
	
	private Point pressed = new Point(0, 0);
	private Double trans = new Double(0, 0);

	public FractalWindow() { init(); }
	
	public FractalWindow(int w, int h, double sx, double sy, double tx, double ty, double zf) {
		size = new Double(w, h);
		scale = new Double(sx, sy);
		transform = new Double(tx, ty);
		this.zf = zf;
		init();
	}

	public FractalWindow(int w, int h, double x, double y, double zf) {
		size.setLocation(w, h);
		this.zf = zf;
		init();
		scale.setLocation(half_size.y, half_size.y); // Needs to be Height so picture is not distorted
		transform.setLocation(half_size.x - (x * scale.x), (y * scale.y) + half_size.y);
		System.out.println(scale);
	}

	private void init() {
		half_size = new Double(size.x / 2, size.y / 2);
	}

	public Double getWindowPoint(int x, int y) {
		return new Point2D.Double(((double)x - transform.x) / scale.x, ((double)y - transform.y) / scale.y);
	}

	public void zoomIn() {
		double x = (transform.x - half_size.x) / scale.x;
		double y = (transform.y - half_size.y) / scale.y;

		scale.x /= zf;
		scale.y /= zf;
		
		transform.x = ((x * scale.x) + half_size.x);
		transform.y = ((y * scale.y) + half_size.y);
	}

	public void zoomOut() {
		double x = (transform.x - half_size.x) / scale.x;
		double y = (transform.y - half_size.y) / scale.y;
		
		scale.x *= zf;
		scale.y *= zf;
		
		transform.x = ((x * scale.x) + half_size.x);
		transform.y = ((y * scale.y) + half_size.y);
	}

	public void pressed(int x, int y) {
		pressed.setLocation(x, y);
		trans.setLocation(transform.x, transform.y);
	}

	public void dragged(int x, int y) {
		transform.x = trans.x + (x - pressed.x);
		transform.y = trans.y + (y - pressed.y);
		System.out.println("New Point: " + transform);
	}

}
