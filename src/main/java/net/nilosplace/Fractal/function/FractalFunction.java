package net.nilosplace.Fractal.function;

import java.awt.Point;

import net.nilosplace.Point3D;

public abstract class FractalFunction implements FractalFunctionInterface {

	protected FractalFunctionParameters params;
	
	public FractalFunction(FractalFunctionParameters params) {
		this.params = params;
	}
	
	protected abstract Point3D computeFractal(Point.Double p);
	
	public Point3D computePoint(Point.Double p) {
		Point3D pr = computeFractal(p);
		return pr;
	}
	
}
