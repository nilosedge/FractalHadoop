package net.nilosplace.Fractal.function;

import java.awt.Point;

import net.nilosplace.Point3D;

public interface FractalFunctionInterface {

	public Point3D computePoint(Point.Double p);
}
