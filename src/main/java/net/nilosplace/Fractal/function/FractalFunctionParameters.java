package net.nilosplace.Fractal.function;

public class FractalFunctionParameters {

	public double maxIter;
	public double p1;
	public double p2;
	public double infinity = 65535;

}
