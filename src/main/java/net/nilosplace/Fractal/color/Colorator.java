package net.nilosplace.Fractal.color;

import java.awt.Color;
import java.util.HashMap;

import net.nilosplace.Point3D;

public class Colorator {

	public enum Palette {
		HUE,
		RAINBOW,
		GRAY
		;
	}

	private Palette pal;
	private HashMap<Palette, Color[]> colorMap = new HashMap<Palette, Color[]>();
	private int colorMax = 256;
	private Color[] colors = null;
	private int color_len = 0;

	public Colorator(Palette pal) {
		this.pal = pal;
		initColors();
	}

	public Color getMUColor(Point3D p, double maxIter) {
		double mu = p.z + 1 - (Math.log(Math.abs(Math.log((p.x * p.x) + (p.y * p.y)))) / Math.log(2));
		mu = (mu / (maxIter + 1));
		if(mu < 0) mu = 0;
		if(mu > 1) mu = 1;
		return colors[(int)(mu * color_len)];
	}


	private void initColors() {
		/*
		000000 Black
		FF0000 Red
		FFFF00 Yellow
		0FFFF0 Green
		00FFFF Cyan
		0000FF Blue
		FF00FF Magenta
		000000 Black
		 */

		Color[] local_colors = new Color[colorMax*7];
		for(int i = 0; i < colorMax; i++) {
			local_colors[i + (colorMax * 0)] = new Color(i, 0, 0);
			local_colors[i + (colorMax * 1)] = new Color((colorMax-1), i, 0);
			local_colors[i + (colorMax * 2)] = new Color((colorMax-1)-i, (colorMax-1), 0);
			local_colors[i + (colorMax * 3)] = new Color(0, (colorMax-1), i);
			local_colors[i + (colorMax * 4)] = new Color(0, (colorMax-1)-i, (colorMax-1));
			local_colors[i + (colorMax * 5)] = new Color(i, 0, (colorMax-1));
			local_colors[i + (colorMax * 6)] = new Color((colorMax-1)-i, 0, (colorMax-1)-i);
		}
		colorMap.put(Palette.RAINBOW, local_colors);

		int max = 1000;
		local_colors = new Color[max];
		for (int i = 0; i < max; i++) {
			local_colors[i] = new Color(Color.HSBtoRGB(i/256f, 1, i/(i+8f)));
		}
		colorMap.put(Palette.HUE, local_colors);

		local_colors = new Color[colorMax];
		for (int i = 0; i < colorMax; i++) {
			local_colors[i] = new Color(255 - i, 255 - i, 255 - i);
		}
		colorMap.put(Palette.GRAY, local_colors);

		colors = colorMap.get(pal);
		color_len = (colors.length - 1);
		//System.out.println(colorMap.get(Palette.RAINBOW).length);
	}

}
