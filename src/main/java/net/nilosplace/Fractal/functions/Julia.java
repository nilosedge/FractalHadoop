package net.nilosplace.Fractal.functions;

import java.awt.Point;

import net.nilosplace.Point3D;
import net.nilosplace.Fractal.function.FractalFunction;
import net.nilosplace.Fractal.function.FractalFunctionParameters;

public class Julia extends FractalFunction {
	
	/*
	 * Z = X/(X^2 + Y^2), Y/(X^2 + Y^2)
	 * 
	 * X = (1 / X) + (X / Y^2)
	 * Y = (Y / X^2) + (1 / Y)
	 */
	
	public Julia(FractalFunctionParameters params) {
		super(params);
	}

	public Point3D computeFractal(Point.Double p) {

		double xc = p.x;
		double yc = p.y;
		double xt = 0;

		int iteration = 0;
		boolean lessThen = false;
		double xcs = 0;
		double ycs = 0;
		
		do {
			xcs = xc * xc;
			ycs = yc * yc;
			xt = (xcs - ycs) + params.p1;
			yc = (yc * xc * 2) + params.p2;
			xc = xt;
			lessThen = (xcs + ycs) <= params.infinity;
		} while(lessThen && ++iteration <= params.maxIter);
		
		return new Point3D(xc, yc, iteration-1);
	}
}