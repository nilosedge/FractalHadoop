package net.nilosplace.Fractal.functions;

import java.awt.Point;

import net.nilosplace.Point3D;
import net.nilosplace.Fractal.function.FractalFunction;
import net.nilosplace.Fractal.function.FractalFunctionParameters;

public class Function2 extends FractalFunction {
	
	/*
	 * Z^3 + C
	 * (x + yi) = ((X^2 + 2xyi - Y^2)(x + yi) + (a + bi)
	 * (x + yi) = (X^3 + 3X^2yi - 3XY^2 - Y^3i)
	 * 
	 * x = X^3 - 3XY^2 + a
	 * y = 3X^2y - Y^3 + bi
	 * 
	 */
	
	public Function2(FractalFunctionParameters params) {
		super(params);
	}
	
	public Point3D computeFractal(Point.Double p) {

		double xc = params.p1;
		double yc = params.p2;
		double xt = 0;

		int iteration = 0;
		boolean lessThen = false;
		double xc2 = 0;
		double yc2 = 0;
		double xc3 = 0;
		double yc3 = 0;
		
		do {
			
			xc2 = xc * xc;
			yc2 = yc * yc;
			xc3 = xc2 * xc;
			yc3 = yc2 * yc;
			
			xt = (xc3 - (3 * xc * yc2)) + p.x;
			yc = ((3 * xc2 * yc) - yc3) + p.y;
			xc = xt;
			lessThen = (xc + yc) <= params.infinity;
		} while(lessThen && iteration++ < params.maxIter);

		return new Point3D(xc, yc, iteration);
	}
}