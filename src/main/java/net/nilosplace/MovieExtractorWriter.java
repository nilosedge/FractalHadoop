package net.nilosplace;

import java.io.File;

import javax.imageio.ImageIO;

import org.jboss.netty.util.internal.ConcurrentHashMap;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;

public class MovieExtractorWriter extends Thread {

	private String file;
	private int count;
	private ConcurrentHashMap<Long, MovieExtractorQueueObject> queue;

	public MovieExtractorWriter(String file, ConcurrentHashMap<Long, MovieExtractorQueueObject> queueout) {
		this.file = file;
		this.queue = queueout;
	}

	public void run() {
		int currentCount = 0;

		File outputDir = new File("/home/www/html/" + file + "_output");
		if(!outputDir.exists()) { outputDir.mkdir(); }
		File movie = new File(outputDir + "/" + file + ".mp4");
		boolean movieExists = movie.exists();

		try {

			PngsToMovie ptm = null;
			if(!movieExists) {
				ptm = new PngsToMovie(movie);
			}

			while(count == 0 || currentCount < count) {

				MovieExtractorQueueObject o = null;
				while(true) {
					o = queue.remove((long)currentCount);
					if(o == null) {
						System.out.println("Object was null sleeping: " + currentCount);
						System.out.println("Keys: " + queue.keySet());
						sleep(1000);
					} else {
						break;
					}
				}

				File outfile = new File(outputDir + "/" + String.format("%05d", currentCount++) + ".png");
				if(!outfile.exists()) {
				//	ImageIO.write(o.getImage(), "png", outfile);
				}
				
				if(ptm != null) ptm.encodeNativeFrame(AWTUtil.fromBufferedImage(o.getImage()));
				
			}
			if(ptm != null) ptm.finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	public void setCount(int count) {
		this.count = count;
	}
}
