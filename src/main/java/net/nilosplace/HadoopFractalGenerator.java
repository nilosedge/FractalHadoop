package net.nilosplace;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.nilosplace.Fractal.color.Colorator;
import net.nilosplace.Fractal.color.Colorator.Palette;
import net.nilosplace.Fractal.function.FractalFunction;
import net.nilosplace.Fractal.function.FractalFunctionParameters;
import net.nilosplace.Fractal.functions.Mandelbrot;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.BasicConfigurator;

public class HadoopFractalGenerator extends Configured implements Tool {

	private static final Log log = LogFactory.getLog(HadoopFractalGenerator.class);

	public static class HadoopFractalMapper extends Mapper<LongWritable, Text, LongWritable, BytesWritable> {

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			System.out.println(value);
			String[] split = value.toString().split("\t+");

			int w = Integer.parseInt(split[1]);
			int h = Integer.parseInt(split[2]);
			
			BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_BGR);

			Graphics2D g = bi.createGraphics();

			double sx = Double.parseDouble(split[3]);
			double sy = Double.parseDouble(split[4]);
			double tx = Double.parseDouble(split[5]);
			double ty = Double.parseDouble(split[6]);
			
			FractalFunctionParameters params = new FractalFunctionParameters();
			params.maxIter = Double.parseDouble(split[7]);
			
			FractalFunction f = new Mandelbrot(params);
			Colorator co = new Colorator(Palette.RAINBOW);
			
			for(int i = 0; i < w; i++) {
				for(int j = 0; j < h; j++) {
					Point3D p = f.computePoint(new Point2D.Double(((double)i - tx) / sx, ((double)j - ty) / sy));
					double nsmooth = p.z + 1 - Math.log(Math.abs(Math.log((p.x * p.x) + (p.y * p.y))))/Math.log(2);
					//g.setColor(co.getColor((nsmooth / (params.maxIter + 1))));
					//g.setColor(co.getColor(p.z));
					g.fillRect(i, j, 1, 1);
				}
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bi, "png", baos);
			baos.flush();
			context.write(key, new BytesWritable(baos.toByteArray()));
			baos.close();
		}
	}

	public static class HadoopFractalReducer extends Reducer<LongWritable, BytesWritable, LongWritable, BytesWritable> {

		public void reduce(LongWritable key, Iterable<BytesWritable> values, Context context) throws IOException, InterruptedException {
			for (BytesWritable val : values) {
				context.write(key, val);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		BasicConfigurator.configure();
		int res = ToolRunner.run(new Configuration(), new HadoopFractalGenerator(), args);
		System.exit(res);
	}

	public int run(String[] args) throws Exception {

		Job job = Job.getInstance(getConf());

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[0] + "_output"));

		job.setJobName("HadoopFractalGenerator");

		job.setJarByClass(HadoopFractalGenerator.class);
		job.setMapperClass(HadoopFractalMapper.class);
		
		job.setCombinerClass(HadoopFractalReducer.class);
		job.setReducerClass(HadoopFractalReducer.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(BytesWritable.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		// Puts all the data into 1 file
		job.setNumReduceTasks(1);
		TextInputFormat.setMaxInputSplitSize(job, 2500);

		job.setInputFormatClass(TextInputFormat.class);

		boolean jobComplete = job.waitForCompletion(true);

		return jobComplete ? 0 : 1;

	}
}
