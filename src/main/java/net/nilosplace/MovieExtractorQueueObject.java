package net.nilosplace;

import java.awt.image.BufferedImage;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MovieExtractorQueueObject {

	private long key;
	private byte[] arrayData;
	private BufferedImage image;
	
	public MovieExtractorQueueObject(long key, byte[] arrayData) {
		this.key = key;
		this.arrayData = arrayData;
	}	

}
