package net.nilosplace;

import java.io.IOException;
import java.util.Map;

import nl.basjes.parse.core.Parser;
import nl.basjes.parse.core.exceptions.DissectionFailure;
import nl.basjes.parse.core.exceptions.InvalidDissectorException;
import nl.basjes.parse.core.exceptions.MissingDissectorsException;
import nl.basjes.parse.httpdlog.ApacheHttpdLoglineParser;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.BasicConfigurator;

public class ApacheLogAnalyzer extends Configured implements Tool {

	public static class ApacheLogMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			Parser<ApacheLogRecord> parser = new ApacheHttpdLoglineParser<>(ApacheLogRecord.class, "combined"); 
			ApacheLogRecord record = new ApacheLogRecord(); 

			try {
				parser.parse(record, value.toString());
				Map<String, String> results = record.getResults();
				
				String host = results.get("IP:connection.client.host");

				context.write(new Text(host), new LongWritable(1));
			} catch (DissectionFailure e) {
				context.write(new Text("failure"), new LongWritable(1));
			} catch (InvalidDissectorException e) {
				context.write(new Text("failure"), new LongWritable(1));
			} catch (MissingDissectorsException e) {
				context.write(new Text("failure"), new LongWritable(1));
			}
		}
	}
	public static class ApacheLogReducer extends Reducer<Text, LongWritable, Text, LongWritable> {

		private LongWritable total = new LongWritable();
		
		public void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
			
			long sum = 0;
			for (LongWritable value : values) {
				sum += value.get();
			}
			total.set(sum);
			context.write(key, total);
		}
	}

	public static void main(String[] args) throws Exception {
		BasicConfigurator.configure();
		int res = ToolRunner.run(new Configuration(), new ApacheLogAnalyzer(), args);
		System.exit(res);
	}

	public int run(String[] args) throws Exception {

		Job job = Job.getInstance(getConf());

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setJobName("ApacheLogAnalyzer");

		job.setJarByClass(ApacheLogAnalyzer.class);
		job.setMapperClass(ApacheLogMapper.class);

		job.setCombinerClass(ApacheLogReducer.class);
		job.setReducerClass(ApacheLogReducer.class);

		job.setNumReduceTasks(1);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextInputFormat.setMaxInputSplitSize(job, 1800000);

		job.setInputFormatClass(TextInputFormat.class);

		boolean jobComplete = job.waitForCompletion(true);
		return jobComplete ? 0 : 1;
	}


}
