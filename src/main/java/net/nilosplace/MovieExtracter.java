package net.nilosplace;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.util.ReflectionUtils;
import org.jboss.netty.util.internal.ConcurrentHashMap;

public class MovieExtracter {

	private static final Log log = LogFactory.getLog(MovieExtracter.class);

	public static void main(String[] args) throws IOException, InterruptedException {

		Configuration conf = new Configuration();

		String file = args[0];

		String input = "file:////home/www/html/" + file + "-part-r-00000";

		Path path = new Path(input);

		LinkedBlockingQueue<MovieExtractorQueueObject> queuein = new LinkedBlockingQueue<MovieExtractorQueueObject>(20);
		ConcurrentHashMap<Long, MovieExtractorQueueObject> queueout = new ConcurrentHashMap<Long, MovieExtractorQueueObject>();

		SequenceFile.Reader reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(path));
		LongWritable key = (LongWritable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
		BytesWritable value = (BytesWritable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

		//MovieExtractorWriter writer = new MovieExtractorWriter(args[0], queueout);
		//writer.start();

		//ArrayList<MovieExtractorProcessor> processors = new ArrayList<MovieExtractorProcessor>();

		//for(int i = 0; i < 5; i++) {
		//	MovieExtractorProcessor processor = new MovieExtractorProcessor(queuein, queueout);
		//	processor.start();
		//	processors.add(processor);
		//}

		File outputDir = new File("/home/www/html/" + file + "_output");
		if(!outputDir.exists()) { outputDir.mkdir(); }

		int counter = 0;
		while (reader.next(key, value)) {
			log.info("Reading: key : " + key + " - value : " + value.getBytes().length + " counter: " + counter);
			File outfile = new File(outputDir + "/" + String.format("%05d", counter++) + ".png");
			if(!outfile.exists()) {
				FileOutputStream fos = new FileOutputStream(outfile);
				fos.write(value.getBytes());
				fos.close();
			}
			//queuein.offer(new MovieExtractorQueueObject(counter, value.getBytes().clone()), 10, TimeUnit.DAYS);
			//counter++;
		}
		IOUtils.closeStream(reader);

		//writer.setCount(counter);

		//for(MovieExtractorProcessor p: processors) {
		//	p.join();
		//}
		//writer.join();

	}

}
