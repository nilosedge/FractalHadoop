package net.nilosplace;

public class ZoomOuter {

	public static void main(String[] args) {
		//w h sx sy tx ty c
		//1  2560  1600  559912.9782739371 734885.7839845422 782546.677600376  14111.51836547885 2
		
//		if(args.length < 6) {
//			usage();
//		}
//		int w = Integer.parseInt(args[0]);
//		int h = Integer.parseInt(args[1]);
//		int iter = Integer.parseInt(args[2]);
//		int zoomLevel = Integer.parseInt(args[3]);
//		
//		double zoomPointX = Double.parseDouble(args[4]);
//		double zoomPointY = Double.parseDouble(args[5]);
		
		int w = 1920;
		int h = 1080;
		int iter = 6000;
		int zoomLevel = 3000;
		
		double zoomPointX = -0.5456675313676621;
		double zoomPointY = -0.6549830220688354;
		
		double zf = 1.011;
		
		int halfw = w / 2;
		int halfh = h / 2;
		
		// Original Video
		//double zoomPointX = 0.0016437219687502394;
		//double zoomPointY = -0.8224676332993273;

		double sx = (h/2);
		double sy = (h/2);
		double tx = halfw - (zoomPointX * sx);
		double ty = ((zoomPointY * sy) + halfh);
		
		for(int i = 0; i < zoomLevel; i++) {

			double x = (tx - halfw) / sx;
			sx *= zf;
			tx = ((x * sx) + halfw);
			
			double y = (ty - halfh) / sy;
			sy *= zf;
			ty = ((y * sy) + halfh);
			
			iter = (int)(Math.sqrt(Math.sqrt(sx)) + 200);
			System.out.println((i + 1) + "\t" + w + "\t" + h + "\t" + sx + "\t" + sy + "\t" + tx + "\t" + ty + "\t" + iter);
		}
	}

	private static void usage() {
		System.out.println("Width Height Iteration Zoom X Y");
		System.exit(1);
	}
}
