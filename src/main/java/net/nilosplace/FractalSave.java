package net.nilosplace;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import net.nilosplace.Fractal.color.Colorator;
import net.nilosplace.Fractal.color.Colorator.Palette;
import net.nilosplace.Fractal.function.FractalFunction;
import net.nilosplace.Fractal.function.FractalFunctionParameters;
import net.nilosplace.Fractal.function.FractalWindow;
import net.nilosplace.Fractal.functions.Julia;

public class FractalSave {

	// Image from desktop
	//private int w = 1280;
	//private int h = 800;
	
	// Balrog Laptop screen size
	private int w = 1680;
	private int h = 1050;
	
	//5823 x 4465 - puzzle
	//private int w = 5823;
	//private int h = 4465;

	// No sure
	//private int w = 2880;
	//private int h = 1800;

	//(2880 x 1800)
	// Desktop Image
	//private FractalWindow window = new FractalWindow(w, h, 559912.9782739371, 734885.7839845422, 783500, 13700, 1.2);
	private FractalWindow window = new FractalWindow(w, h, 0, 0, 1.2); // Julia Set
	private Colorator co = new Colorator(Palette.RAINBOW);
	private Point2D.Double[][] windowPoints = new Point2D.Double[w][h];

	public static void main(String args[]) {
		new FractalSave();
	}

	public FractalSave() {

		System.out.println("Computing Window Points");
		for(int i = 0; i < w; i++) {	
			for(int j = 0; j < h; j++) {
				windowPoints[i][j] = window.getWindowPoint(i, j);
			}
		}

		System.out.println("Finished window points");

		
		//new FractalCreator(110, 650).start();
		//new FractalCreator(140, 650).start();
		//new FractalCreator(470, 540).start();
		//new FractalCreator(600, 420).start();
		//new FractalCreator(690, 280).start();
		////new FractalCreator(700, 270).start(); // Picture from desktop
		//new FractalCreator(770, 100).start();
		//new FractalCreator(930, 240).start();
		//new FractalCreator(990, 250).start();

		try {
			ThreadPoolExecutor executor = new ThreadPoolExecutor(40, 40, 0L, TimeUnit.MILLISECONDS, new LimitedQueue<Runnable>(200));
			//ExecutorService executor = Executors.newFixedThreadPool(8);

			//params.p1 = -0.7; // Julia Set
			//params.p2 = 0.27015; // Julia Set

			// -0.11 x 0.65
			// -0.14 x 0.65
			// -0.47 x 0.54
			// -0.60 x 0.42
			// -0.69 x 0.28
			// -0.77 x 0.10
			// -0.93 x 0.24
			// -0.99 x 0.25

			//params.p2 = 0.23000; // Julia Set


			//		IntStream.range(0, w).parallel().forEach(i -> {
			//			System.out.println(i);
			//		});

			for(int ppi = 0; ppi <= 1000; ppi++) {
				for(int ppj = 0; ppj <= 1000; ppj++) {
					FractalCreator creator = new FractalCreator(ppi, ppj);
					executor.execute(creator);
					//System.out.println("Adding New Creator");
				}
			}

			executor.shutdown();  
			while (!executor.isTerminated()) {
				Thread.sleep(1000);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}


	}


	public class FractalCreator extends Thread {

		private int p1, p2;

		public FractalCreator(int p1, int p2) {
			this.p1 = p1;
			this.p2 = p2;
		}

		public void run() {
			File outputDir = new File("/Users/olinblodgett/Desktop/fractal/images/" + p2);
			if(!outputDir.exists()) {
				outputDir.mkdirs();
			}
			File outputfile = new File("/Users/olinblodgett/Desktop/fractal/images/" + p2 + "/" + p1 + "x" + p2 + ".png");

			if(!outputfile.exists()) {

				BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_BGR);
				Graphics2D g = bi.createGraphics();
				g.setBackground(Color.BLACK);

				FractalFunctionParameters params = new FractalFunctionParameters();
				FractalFunction f = new Julia(params);
				params.p1 = -(p1 / 1000.0);
				params.p2 = (p2 / 1000.0);
				params.maxIter = 200;

				for(int i = 0; i < w; i++) {	
					for(int j = 0; j < h; j++) {
						Point3D p = f.computePoint(windowPoints[i][j]);
						//Point3D p = f.computePoint(window.getWindowPoint(i, j));
						g.setColor(co.getMUColor(p, params.maxIter));
						g.fillRect(i, j, 1, 1);
					}
				}

				g.dispose();

				//File outputfile = new File("/Users/balrog/Desktop/fractal/" + ppi + "x" + ppj + ".png");
				try {
					ImageIO.write(bi, "png", outputfile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Finished: " + p1 + "x" + p2);
			} else {
				System.out.println("File Exists: " + p1 + "x" + p2);
			}
		}

	}

	public class LimitedQueue<E> extends LinkedBlockingQueue<E> {
		public LimitedQueue(int maxSize) {
			super(maxSize);
		}

		@Override
		public boolean offer(E e) {
			// turn offer() and add() into a blocking calls (unless interrupted)
			try {
				put(e);
				return true;
			} catch(InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
			return false;
		}

	}


}
